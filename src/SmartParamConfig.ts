export type SmartParamConfig = { 
    preserveParams?: boolean
    preserveQueryParams?: boolean
}

export const defaultSmartParamConfig = { 
    preserveParams: true, 
    preserveQueryParams: true
} as const

export type PreserveBothParams = { preserveParams?: true, preserveQueryParams?: true }
export type PreserveParamsOnly = { preserveParams?: true, preserveQueryParams: false }
export type PreserveQueryParamsOnly = { preserveParams: false, preserveQueryParams?: true }
export type PreserveNeitherParams = { preserveParams: false, preserveQueryParams: false }
