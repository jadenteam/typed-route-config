
export type ParamName = { name: string, optional: boolean }

export type Route<P, Q> = {
    renderPath: (partialParams: Partial<P>) => string
    renderRoute: (params: P) => string
    renderQueryString: (queryParams: Q) => string
    paramNames: ParamName[]
    queryParamNames: (keyof Q)[]
}

export const emptyRoute: Route<{}, {}> = {
    renderPath: () => '',
    renderRoute: () => '',
    renderQueryString: () => '',
    paramNames: [], 
    queryParamNames: []
}

export const makeParamName = (name: string, optional: boolean = false) => ({ name, optional })