import { useMatch, useParams as useParamsReactRouter } from 'react-router-dom'

import { RoutesAPI } from '.././RoutesAPI'
import { RouteParams, QueryParams } from '.././GetParams'
import { RouteConfig } from '.././RouteConfig'
import { RequiredKeys, OptionalKeys, CleanIntellisense } from '.././typescript'
import { makeAssertNoMissingParams } from '.././makeAssertNoMissingParams'
import { UseQueryParams } from './makeUseQueryParams'
import { SmartParamConfig, defaultSmartParamConfig, PreserveBothParams, PreserveParamsOnly, PreserveQueryParamsOnly, PreserveNeitherParams } from '.././SmartParamConfig'

type KeysThatCanBeOptional<TCurrentParams, TNextParams> = Extract<keyof TNextParams, OptionalKeys<TNextParams> | RequiredKeys<TCurrentParams>>

// this does not account for the types of values at the keys (e.g. it just assumes TCurrentParams[K] === TNextParams[K] where K is a specific key both param types share)
// is technically fixable but not super important, the user would really need to be mis-using this to 
// find an issue (e.g. having 2 params with same key, but different types AND THEN trying to use the magic of this hook to assign one to the other)
type _PartialOfParamsWeAlreadyHave<TCurrentParams, TNextParams> = CleanIntellisense<{
    [K in KeysThatCanBeOptional<TCurrentParams, TNextParams>]?: TNextParams[K]
} & {
    [K in Exclude<keyof TNextParams, KeysThatCanBeOptional<TCurrentParams, TNextParams>>]: TNextParams[K]
}>

type PartialOfParamsWeAlreadyHave<R extends RouteConfig, TCurrentRouteName extends keyof R, N extends keyof R> = 
    _PartialOfParamsWeAlreadyHave<
        RouteParams<R, TCurrentRouteName>, // these are the params we already have (from current route context)
        RouteParams<R, N> // these are the params we want to make partial, but only the params which are required params in the current route context
    >

type KeysOfFullyOptional<R extends RouteConfig, TCurrentRouteName extends keyof R> = {
    [N in keyof R]: {} extends PartialOfParamsWeAlreadyHave<R, TCurrentRouteName, N> ? N : never
}[keyof R];


type SmartRoute<R extends RouteConfig, TCurrentRouteName extends keyof R> = {
    <N extends keyof R>(routeName: N, givenParams: PartialOfParamsWeAlreadyHave<R, TCurrentRouteName, N>, givenQueryParams?: QueryParams<R, N>): string
    <N extends KeysOfFullyOptional<R, TCurrentRouteName>>(routeName: N): string
}

export type LenientSmartRoute<R extends RouteConfig> = 
    <N extends keyof R>(routeName: N, givenParams?: Partial<RouteParams<R, N>>, givenQueryParams?: QueryParams<R, N>) => string


type StrictSmartRoute<R extends RouteConfig> = 
    <N extends keyof R>(routeName: N, givenParams: RouteParams<R, N>, givenQueryParams?: QueryParams<R, N>) => string

export type UseSmartRoute<R extends RouteConfig> = {

    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName): SmartRoute<R, TCurrentRouteName>
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName, smartParamConfig: PreserveBothParams | PreserveParamsOnly): SmartRoute<R, TCurrentRouteName>

    // if the Params arent being preserved, then smartRoute is strict and demands all params for the routeName passed to it when called
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName, smartParamConfig: PreserveQueryParamsOnly): StrictSmartRoute<R>

    // if no currentRouteName is passed through, the smartRoute function becomes lenient as long as Params are being preserved
    (): LenientSmartRoute<R>
    (smartParamConfig: PreserveBothParams | PreserveParamsOnly): LenientSmartRoute<R>

    // if the Params arent being preserved, then smartRoute is strict and demands all params for the routeName passed to it when called
    (smartParamConfig: PreserveQueryParamsOnly): StrictSmartRoute<R>

    // want to signal that passing false false is dumb (just use the normal route function, no need for smartRoute)
    (smartParamConfig: PreserveNeitherParams): unknown
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName, smartParamConfig: PreserveNeitherParams): unknown
}

export function makeUseSmartRoute<R extends RouteConfig>(route: RoutesAPI<R>['route'], path: RoutesAPI<R>['path'], paramNames: RoutesAPI<R>['paramNames'], useQueryParams: UseQueryParams<R>): UseSmartRoute<R> {

    //https://github.com/microsoft/TypeScript/issues/34523
    const fn = makeAssertNoMissingParams<R>(paramNames, 'useSmartRoute')
    const assertNoMissingParams: typeof fn = fn

    return function useSmartRoute<TCurrentRouteName extends keyof R>(currentRouteNameOrSmartParamConfig?: TCurrentRouteName | SmartParamConfig, maybeSmartParamConfig?: SmartParamConfig): any {

        const currentRouteName: TCurrentRouteName | undefined = typeof currentRouteNameOrSmartParamConfig === 'string' 
            ? currentRouteNameOrSmartParamConfig 
            : undefined

        const { 
            preserveParams = defaultSmartParamConfig.preserveParams,
            preserveQueryParams = defaultSmartParamConfig.preserveQueryParams
        } = maybeSmartParamConfig || (
            typeof currentRouteNameOrSmartParamConfig === 'object' 
            ? currentRouteNameOrSmartParamConfig
            : {}
        )

        // we need to handle both scenarios: where they provided the current route name and where they did not
        // if they did provide it, then we can use that to perform the match using useMatch
        // if not, then we rely on useParamsReactRouter
        // but since this is a hook, we must call both hooks every render for safety

        const match = useMatch({
            path: currentRouteName ? path(currentRouteName) : '',
            end: false
        })
        const paramsUsingRouteName = match?.params || {}
        const paramsFromContext = useParamsReactRouter<RouteParams<R, TCurrentRouteName>>()

        const applicableParams = currentRouteName ? paramsUsingRouteName : paramsFromContext


        const [queryParamsFromContext] = useQueryParams(currentRouteName as any) // useQueryParams is capable of handling any routeName (including undefined) the type is just slightly more restrictive

        return function smartRoute<N extends keyof R>(routeName: N, givenParams?: PartialOfParamsWeAlreadyHave<R, TCurrentRouteName, N>, givenQueryParams?: QueryParams<R, N>) {

            const params = {
                ...(preserveParams ? applicableParams : {}),
                ...(givenParams || {})
            }
            const msg = currentRouteName ? `Unable to get all params from routeName=${String(currentRouteName)} for routeName=${String(routeName)}` : ''
            assertNoMissingParams(routeName, params, msg)

            const queryParams = {
                ...(preserveQueryParams ? queryParamsFromContext : {}),
                ...(givenQueryParams || {})
            }

            return route(routeName, params, queryParams as any)
        }
    }
}
