import { useMatch, PathPattern as PathPatternReactRouter, PathMatch } from 'react-router-dom'

import { RouteConfig } from '../RouteConfig'
import { RouteParams } from '../GetParams'
import { Modify } from '../typescript'
import { RoutesAPI } from '../RoutesAPI'

type PathPattern = Omit<PathPatternReactRouter, 'path'>

type RouteArg<R extends RouteConfig, M extends keyof R> = {
    [N in keyof R]: { routeName: N, pattern: PathPattern, params: RouteParams<R, N> }
}[M]

type TypedPathMatch<R extends RouteConfig, N extends keyof R> = Modify<PathMatch, RouteArg<R, N>>

type TypedPathPattern<R extends RouteConfig, N extends keyof R> = Modify<PathPattern, {
    routeName: N
}>
// N1 type param is to let the end user specify their own alias for RouteName for the sake of intellisense, it doesnt actually mean anything 
export type UseRouteMatch<R extends RouteConfig, N1 extends keyof R = keyof R> = <N extends N1>(routeNameOrPattern: N | TypedPathPattern<R, N>) => TypedPathMatch<R, N> | null

export function makeUseRouteMatch<R extends RouteConfig>(path: RoutesAPI<R>['path']): UseRouteMatch<R> {
    return function useRouteMatch(routeNameOrPattern) {
    
        const routeMatchArg = typeof routeNameOrPattern !== 'object'
            ? {
                path: path(routeNameOrPattern),
                routeName: routeNameOrPattern
            }
            : {
                ...routeNameOrPattern,
                path: path(routeNameOrPattern.routeName)
            }

        const match = useMatch(routeMatchArg)

        if (match === null) return null

        
        // here we know we have matchedRouteName = 'routeName1' | 'routeName2' and match.params = params1 | params2
        // but we need to assert that if we have 'routeName1' then we have params1 and if 'routeName2' then params2
        // could do some fancy checks using paramNames etc. but for now just assert its the case
        
        // first assign to something that pretty much looks like the result,
        // but without the generic context so we can at least get an error if we miss one of the properties
        const result: TypedPathMatch<RouteConfig, string> = {
            ...match,
            routeName: routeMatchArg.routeName as string,
        }

        // now just cast it
        return result as any
    }
}
