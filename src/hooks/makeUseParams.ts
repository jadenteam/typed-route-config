import { useMatch } from 'react-router-dom'

import { RoutesAPI } from '.././RoutesAPI'
import { RouteParams } from '.././GetParams'
import { RouteConfig } from '.././RouteConfig'
import { makeAssertNoMissingParams } from '.././makeAssertNoMissingParams'
import { IfHasKeys } from '../typescript'

type KeysWithParams<R extends RouteConfig> = {
    [N in keyof R]: IfHasKeys<RouteParams<R, N>, N, never>
}[keyof R]

export type UseParams<R extends RouteConfig> = <N extends KeysWithParams<R>>(routeName: N, throwOnMissingParams?: boolean) => RouteParams<R, N>

export function makeUseParams<R extends RouteConfig>(path: RoutesAPI<R>['path'], paramNames: RoutesAPI<R>['paramNames']): UseParams<R> {

    //https://github.com/microsoft/TypeScript/issues/34523
    const fn = makeAssertNoMissingParams<R>(paramNames, 'useParams')
    const assertNoMissingParams: typeof fn = fn

    return function useParams<N extends KeysWithParams<R>>(routeName: N, throwOnMissingParams: boolean = false) {

        const match = useMatch({
            path: path(routeName),
            end: false
        }) as RouteParams<R, N> | null

        const paramsFromContext = match?.params || {}

        if (throwOnMissingParams) {
            assertNoMissingParams(routeName, paramsFromContext)
        }
        
        return paramsFromContext as any
    }
}