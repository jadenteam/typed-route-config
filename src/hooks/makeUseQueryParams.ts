import { useSearchParams, NavigateOptions } from 'react-router-dom'

import { RoutesAPI } from '.././RoutesAPI'
import { QueryParams } from '.././GetParams'
import { RouteConfig } from '.././RouteConfig'
import { IfHasKeys } from '../typescript'

export type KeysWithQueryParams<R extends RouteConfig> = {
    [N in keyof R]: IfHasKeys<QueryParams<R, N>, N, never>
}[keyof R]

type StringParams = { 
    [queryParamName: string]: string | undefined
}

export type UseQueryParams<R extends RouteConfig> = {
    <N extends KeysWithQueryParams<R>>(routeName: N): [QueryParams<R, N>, SetURLSearchParams<QueryParams<R, N>>]
    <T extends StringParams = StringParams>(): [T, SetURLSearchParams<T>]
}

type Options = NavigateOptions & {
    mergeParams?: boolean
}

type SetURLSearchParams<P> = 
    (nextInit?: Partial<P> | ((prev: Partial<P>) => Partial<P>), options?: Options) => void;

export function makeUseQueryParams<R extends RouteConfig>(queryParamNames: RoutesAPI<R>['queryParamNames']): UseQueryParams<R> {
    return function useQueryParams<N extends keyof R>(routeName?: N): [QueryParams<R, N>, SetURLSearchParams<QueryParams<R, N>>] {

        const reduceUrlSearchParams = (urlSearchParams: URLSearchParams) => {
            const names = routeName ? queryParamNames(routeName) : Array.from(urlSearchParams.keys())

            return names.reduce((result, queryParamName) => {
                const queryParamNameStr = queryParamName.toString()
                if (urlSearchParams.has(queryParamNameStr)) {
                    const queryParam = urlSearchParams.get(queryParamNameStr)
                    if (queryParam !== null) {
                        result[queryParamName] = queryParam
                    }
                }
                return result
            }, {} as QueryParams<R, N>)
        }
        
        const [urlSearchParams, setUrlSearchParams] = useSearchParams()

        return [
            reduceUrlSearchParams(urlSearchParams),
            (nextInit, options) => setUrlSearchParams(prevUrlSearchParams => {
                const prevParams = reduceUrlSearchParams(prevUrlSearchParams)

                const newParams = typeof nextInit === 'function'
                    ? nextInit(prevParams)
                    : nextInit

                const nextParams = options?.mergeParams
                    ? {
                        ...prevParams,
                        ...newParams,
                    }
                    : newParams

                return nextParams as Record<string, string>
            }, options)
        ]
        
    }
}
