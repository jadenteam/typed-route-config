
export type CleanIntellisense<T> = {} & { [K in keyof T]: T[K] }
export type RequiredKeys<T> = { [K in keyof T]-?: {} extends Pick<T, K> ? never : K }[keyof T]
export type OptionalKeys<T> = { [K in keyof T]-?: {} extends Pick<T, K> ? K : never }[keyof T]

export type IfHasKeys<T, TTrue, TFalse> = keyof T extends never ? TFalse : TTrue
export type Modify<T, R> = Pick<T, Exclude<keyof T, keyof R>> & R

export type UnionToIntersection<U> =
    (U extends U ? (x: U) => void : never) extends ((x: infer T) => void)
    ? T
    : never

type AllKeys<T> = keyof UnionToIntersection<T>
type GuaranteedKeys<T> = Exclude<keyof T, OptionalKeys<T>>

// NOTE: remember weirdness with intersections if the types at common keys are different (e.g. { a: boolean } & { a: string } -> never)
export type UnionToSpecialIntersection<T> = CleanIntellisense<{
    [K in GuaranteedKeys<T>]: T[K]
} & {
    [K in Exclude<AllKeys<T>, GuaranteedKeys<T>>]?: UnionToIntersection<T>[K]
}>

export type DeepRequired<T> = { [P in keyof T]-?: DeepRequired<T[P]> }