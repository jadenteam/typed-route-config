import React from 'react'

import { MemoryRouter, Routes, Route as RouteComp } from 'react-router-dom'
import { render, fireEvent } from '@testing-library/react'

import { ParamName } from './Route'
import { createRoutes } from './createRoutes'
import { RouteParams, QueryParams } from './GetParams'

import { DeepRequired } from './typescript'
import { RoutesAPI } from './RoutesAPI'

const routesApi = createRoutes(root => ({
    'path': root.path('a'),
    'param': root.param('p0'),
    'param.ofDefault': root.param('dp0').ofDefault<'abc' | 'def'>('abc'),
    'numParam': root.numParam('np0'),

    'path.queryParam': root.path('aa').queryParam('qp0'),
    'path.queryParam.of': root.path('ab').queryParam('qp1').of<'qv1' | 'qv2'>(),
    'path.queryParam.ofDefault': root.path('ac').queryParam('qp2').ofDefault<'qv3' | 'qv4'>('qv3'),

    'path.param':           root.path('b').param('p1'),
    'path.param.of':        root.path('c').param('p2').of<'v1' | 'v2'>(),
    'path.param.of.param':  root.path('d').param('p3').of<'v3' | 'v4'>().param('p4'),

    'path.numParam':                root.path('h').numParam('np1'),
    'path.numParam.numParam':       root.path('j').numParam('np2').numParam('np3'),

    'group': root.path('grouping').param('pgroup').queryParam('qpgroup').group(g => ({
        '': g,
        'path': g.path('ga'),
        'param': g.param('gp0'),
        'numParam': g.numParam('gnp0'),
        'param.queryParam': g.param('gp1').queryParam('gqp0'),
        'nestedGroup': g.path('nested-grouping').param('pnestedgroup').queryParam('qpnestedgroup').group(ng => ({
            '': ng,
            'path': ng.path('nga'),
            'param': ng.param('ngp0'),
            'numParam': ng.numParam('ngnp0'),
            'param.queryParam': ng.param('ngp1').queryParam('ngqp0'),
        }))
    })),


    'longpath': root.path('first/second').path('third').path('fourth'),

    'all': root.queryParam('queryp1').path('aaa').param('ppp').of<0 | 'x' | 'y'>().queryParam('query_missing').numParam('nnn').queryParam('queryp2').of<1 | 2>(),

    // using this to test useSmartRoute
    'some_params': root.param('param_a').param('param_b').queryParam('query_param_a'),
    'some_params_plus': root.param('param_a').param('param_b').param('param_c').param('definitely_mandatory_d').queryParam('query_param_a').queryParam('query_param_b'),
    'covered_by_some_params': root.param('param_a').param('param_b')

}));
type _Routes = typeof routesApi.routes;
interface Routes extends _Routes {}
type RouteName = keyof Routes & string;

const { route, path, routeNames, isRouteName, paramNames, SmartLink, useSmartRoute, useParams, useQueryParams, useRouteMatch }: RoutesAPI<Routes, RouteName> = routesApi

type RouteParamsFor<N extends RouteName> = RouteParams<Routes, N>
type QueryParamsFor<N extends RouteName> = QueryParams<Routes, N>


const makeVal = <N extends RouteName>(
    routeName: N,
    params: RouteParamsFor<N>,
    expectedRoute: string,
    expectedPath: string,
    expectedParamNames: ParamName[],
    extraText?: string,
    queryParams?: QueryParamsFor<N>
) => ({ routeName, params, expectedRoute, expectedPath, expectedParamNames, extraText, queryParams});

const testVals: { 
    [N in RouteName]: { 
        routeName: N, 
        params: RouteParamsFor<N>, 
        expectedRoute: string,
        expectedPath: string,
        expectedParamNames: ParamName[],
        extraText?: string,
        queryParams?: QueryParamsFor<N>
    } 
}[RouteName][] = [
        makeVal('param',            { p0: 'xyz' },  '/xyz', '/:p0',     [{ name: 'p0', optional: false }]),
        makeVal('param.ofDefault',  { dp0: 'def' }, '/def', '/:dp0',    [{ name: 'dp0', optional: true }]),
        makeVal('param.ofDefault',  {},             '/abc', '/:dp0',    [{ name: 'dp0', optional: true }], 'Relying on default value'),

        makeVal('numParam',                 { np0: 666 }, '/666', '/:np0', [{ name: 'np0', optional: false }]),

        makeVal('path.queryParam', {}, '/aa?qp0=abcd', '/aa', [], 'All query params present', { qp0: 'abcd' }),
        makeVal('path.queryParam', {}, '/aa', '/aa', [], 'No query params present'),
        makeVal('path.queryParam.of', {}, '/ab?qp1=qv1', '/ab', [], 'All query params present', { qp1: 'qv1' }),
        makeVal('path.queryParam.ofDefault', {}, '/ac?qp2=qv3', '/ac', [], 'Query param falls back onto default', {}),
        makeVal('path.queryParam.ofDefault', {}, '/ac?qp2=qv4', '/ac', [], 'Query param default is overriden', { qp2: 'qv4' }),

        makeVal('path.param',           { p1: 'abc' },              '/b/abc',       '/b/:p1',       [{ name: 'p1', optional: false }] ),
        makeVal('path.param.of',        { p2: 'v1' },               '/c/v1',        '/c/:p2',       [{ name: 'p2', optional: false }] ),
        makeVal('path.param.of.param',  { p3: 'v3', p4: 'abc' },    '/d/v3/abc',    '/d/:p3/:p4',   [{ name: 'p3', optional: false }, { name: 'p4', optional: false }] ),

        makeVal('path.numParam',            { np1: 666 },           '/h/666',       '/h/:np1',      [{ name: 'np1', optional: false }] ),
        makeVal('path.numParam.numParam',   { np2: 222, np3: 333 }, '/j/222/333',   '/j/:np2/:np3', [{name: 'np2', optional: false }, {name: 'np3', optional: false }] ),

        makeVal('group',        { pgroup: 'ggg' },              '/grouping/ggg',    '/grouping/:pgroup',        [{ name: 'pgroup', optional: false }] ),
        makeVal('group.path',   { pgroup: 'ggg' },              '/grouping/ggg/ga', '/grouping/:pgroup/ga',     [{ name: 'pgroup', optional: false }] ),
        makeVal('group.param',  { pgroup: 'ggg', gp0: 'abc' }, '/grouping/ggg/abc', '/grouping/:pgroup/:gp0',   [{name: 'pgroup', optional: false }, {name: 'gp0', optional: false }] ),

        makeVal('group.numParam',           { pgroup: 'ggg', gnp0: 123 },   '/grouping/ggg/123',            '/grouping/:pgroup/:gnp0',      [{name: 'pgroup', optional: false }, {name: 'gnp0', optional: false }] ),
        makeVal('group.param.queryParam',   { pgroup: 'ggg', gp1: 'abc' },  '/grouping/ggg/abc?qpgroup=qqq1&gqp0=qqq2',   '/grouping/:pgroup/:gp1',       [{name: 'pgroup', optional: false }, {name: 'gp1', optional: false }], 'All query params present', { qpgroup: 'qqq1', gqp0: 'qqq2'}),


        makeVal('group.nestedGroup',        { pgroup: 'ggg', pnestedgroup: 'ngngng' },              '/grouping/ggg/nested-grouping/ngngng',         '/grouping/:pgroup/nested-grouping/:pnestedgroup',     [{ name: 'pgroup', optional: false }, { name: 'pnestedgroup', optional: false }] ),
        makeVal('group.nestedGroup.path',   { pgroup: 'ggg', pnestedgroup: 'ngngng' },              '/grouping/ggg/nested-grouping/ngngng/nga',     '/grouping/:pgroup/nested-grouping/:pnestedgroup/nga',     [{ name: 'pgroup', optional: false }, { name: 'pnestedgroup', optional: false }] ),
        makeVal('group.nestedGroup.param',  { pgroup: 'ggg', pnestedgroup: 'ngngng', ngp0: 'nabc' }, '/grouping/ggg/nested-grouping/ngngng/nabc', '/grouping/:pgroup/nested-grouping/:pnestedgroup/:ngp0',   [{name: 'pgroup', optional: false }, { name: 'pnestedgroup', optional: false }, {name: 'ngp0', optional: false }] ),

        makeVal('group.nestedGroup.numParam',       { pgroup: 'ggg', pnestedgroup: 'ngngng', ngnp0: 123 },    '/grouping/ggg/nested-grouping/ngngng/123',   '/grouping/:pgroup/nested-grouping/:pnestedgroup/:ngnp0',  [{name: 'pgroup', optional: false }, { name: 'pnestedgroup', optional: false }, {name: 'ngnp0', optional: false }] ),

        makeVal(
            'group.nestedGroup.param.queryParam', 
            { pgroup: 'ggg', pnestedgroup: 'ngngng', ngp1: 'abc' }, 
            '/grouping/ggg/nested-grouping/ngngng/abc?qpgroup=def&qpnestedgroup=ghi&ngqp0=jkl',
            '/grouping/:pgroup/nested-grouping/:pnestedgroup/:ngp1',
            [{name: 'pgroup', optional: false },
            { name: 'pnestedgroup', optional: false }, {name: 'ngp1', optional: false }],
            'All query params present',
            { qpgroup: 'def', qpnestedgroup: 'ghi', ngqp0: 'jkl' }
        ),

        makeVal('longpath', {}, '/first/second/third/fourth', '/first/second/third/fourth', [] ),
        
        makeVal('all', { ppp: 0, nnn: 123, }, '/aaa/0/123?queryp1=qqq&queryp2=2', '/aaa/:ppp/:nnn', [{name: 'ppp', optional: false }, {name: 'nnn', optional: false }], '', { queryp1: 'qqq', queryp2: 2 } ),
]

describe("'path' route", () => {
    test("route('path') === '/a'", () => {
        expect(route('path')).toBe('/a');
    })
    test("route('path', {}) === '/a'", () => {
        expect(route('path', {})).toBe('/a');
    })
    test("route('path', {}, {}) === '/a'", () => {
        expect(route('path', {}, {})).toBe('/a');
    })
    test("route('path', { xyz: '987' }, { abc: '123' }) === '/a'", () => {
        expect(route('path', { xyz: '987' }, { abc: '123' })).toBe('/a');
    })

    test("path('path') === '/a'", () => {
        expect(path('path')).toBe('/a');
    })

    // TODO: add more tests for partial path calls like below
    test("path('all', { nnn: 123 }) === '/aaa/:ppp/123'", () => {
        expect(path('all', { nnn: 123 })).toBe('/aaa/:ppp/123')
    })
});

describe("routeNames", () => {
    const expectedRouteNames = [
        'path', 'param', 'param.ofDefault',
        'numParam', 'path.queryParam', 'path.queryParam.of', 'path.queryParam.ofDefault', 'path.param',
        'path.param.of', 'path.param.of.param',
        'path.numParam', 'path.numParam.numParam',

        'group',
        'group.path',
        'group.param',
        'group.numParam',
        'group.param.queryParam',
        'group.nestedGroup',


        'group.nestedGroup.path',
        'group.nestedGroup.param',
        'group.nestedGroup.numParam',
        'group.nestedGroup.param.queryParam',

        'longpath', 'all', 'some_params', 'some_params_plus', 'covered_by_some_params'
    ].sort()

    const sortedRouteNames = [...routeNames].sort()

    test("routeNames has correct keys of routes", () => {
        expect(sortedRouteNames).toEqual(expectedRouteNames);
    })

    test("isRouteName works correctly", () => {
        expect(isRouteName('group.nestedGroup.numParam')).toBe(true)
        expect(isRouteName('fake.route')).toBe(false)
    })
});

testVals.forEach(({ routeName, params, queryParams, expectedRoute, expectedPath, extraText, expectedParamNames }) => {

    describe(`'${routeName}' route${extraText ? ` (${extraText})` : ''}`, () => {
    
        test(`route('${routeName}', ${JSON.stringify(params)}, ${JSON.stringify(queryParams)}) === ${expectedRoute}`, () => {
            expect(route(routeName, params, queryParams)).toBe(expectedRoute);
        })
    
        test(`path('${routeName}') === '${expectedPath}'`, () => {
            expect(path(routeName)).toBe(expectedPath);
        })

        test(`paramNames('${routeName}') === '${JSON.stringify(expectedParamNames)}'`, () => {
            expect(paramNames(routeName)).toEqual(expectedParamNames);
        })
    });

})

describe("useParams and useQueryParams", () => {
    const routeName = 'all' as const

    const TestUseParamsAndUseQueryParams: React.FC = () => {
        const { nnn, ppp } = useParams(routeName)
        const [{ queryp1, queryp2 }, setQueryParams] = useQueryParams(routeName)
        const [{ queryp1: queryp1_v2, queryp2: queryp2_v2 }] = useQueryParams()

        return <>
            <button data-testid="mergeNewQueryp1" onClick={() => {
                setQueryParams({ queryp1: 'def' }, { mergeParams: true })
            }}>mergeNewQueryp1</button>
            <button data-testid="overwrite" onClick={() => {
                setQueryParams({ queryp1: 'ghi' })
            }}>overwrite</button>
            <div data-testid="nnn">{nnn}</div>
            <div data-testid="ppp">{ppp}</div>
            <div data-testid="queryp1">{queryp1}</div>
            <div data-testid="queryp2">{queryp2}</div>
            <div data-testid="queryp1_v2">{queryp1_v2}</div>
            <div data-testid="queryp2_v2">{queryp2_v2}</div>
        </>
    }

    test("useParams and useQueryParams", () => {
        const { getByTestId } = render(
            <MemoryRouter initialEntries={[route(routeName, { nnn: 1, ppp: 'x' }, { queryp1: 'abc', queryp2: 2 })]}>
                <Routes>
                    <RouteComp path={path(routeName)} element={<TestUseParamsAndUseQueryParams />}/>
                </Routes>
            </MemoryRouter>
        )

        expect(getByTestId('nnn')).toHaveTextContent(/^1$/)
        expect(getByTestId('ppp')).toHaveTextContent(/^x$/)
        expect(getByTestId('queryp1')).toHaveTextContent(/^abc$/)
        expect(getByTestId('queryp2')).toHaveTextContent(/^2$/)
        expect(getByTestId('queryp1_v2')).toHaveTextContent(/^abc$/)
        expect(getByTestId('queryp2_v2')).toHaveTextContent(/^2$/)
        
        fireEvent.click(getByTestId('mergeNewQueryp1'))
        // tests setQueryParams with a merge preserves the other query params
        expect(getByTestId('queryp1')).toHaveTextContent(/^def$/)
        expect(getByTestId('queryp2')).toHaveTextContent(/^2$/)

        fireEvent.click(getByTestId('overwrite'))
        // tests setQueryParams without merge overwrites the other query params
        expect(getByTestId('queryp1')).toHaveTextContent(/^ghi$/)
        expect(getByTestId('queryp2')).toHaveTextContent(/^$/)


    })

})


function TestUseHook<T>(props: {
    useHook: () => T,
    handleHookResult: (t: T) => string,
    shouldExitEarly: (t: T) => boolean
}) {
    const hookResult = props.useHook()
        
    if (props.shouldExitEarly(hookResult)) return <></>;

    return <div data-testid="use-hook-testid" className={props.handleHookResult(hookResult)} ></div>
}

function useHookTest<T>(x: {
    testName: string,
    routeName: RouteName, 
    initialRoute: string,
    initialPath?: string,
    useHook: () => T, 
    handleHookResult: (t: T) => string,
    shouldExitEarly?: (t: T) => boolean,
    expected: string
}) {
    test(x.testName, () => {
    
        const { getByTestId } = render(
            <MemoryRouter initialEntries={[x.initialRoute]}>
                <Routes>
                    <RouteComp 
                        path={x.initialPath || path(x.routeName)} 
                        element={
                            <TestUseHook 
                                useHook={x.useHook}
                                handleHookResult={x.handleHookResult}
                                shouldExitEarly={x.shouldExitEarly || (() => false)}
                            />
                        }
                    />
                </Routes>
            </MemoryRouter>
        )

        expect(getByTestId("use-hook-testid")).toHaveClass(x.expected)
    })
}

describe("useParams", () => {

    const routeName = 'group' as const
    const initialRoute = route('group.param', { gp0: 'a', pgroup: 'b' })

    useHookTest({
        testName: `useParams able to extract params using higher up route name`,
        routeName,
        initialRoute,
        initialPath: `${path(routeName)}/*`,
        useHook: () => useParams(routeName),
        handleHookResult: params => params.pgroup,
        expected: 'b'
    })

})

describe("useRouteMatch", () => {

    const routeName = 'all' as const
    const initialRoute = route(routeName, { nnn: 1, ppp: 'x' }, { queryp1: 'abc', queryp2: 2 })

    useHookTest({
        testName: `Tests useRouteMatch with single routeName param`,
        routeName,
        initialRoute,
        useHook: () => useRouteMatch(routeName),
        shouldExitEarly: match => match === null,
        handleHookResult: match => match !== null ? match.params.ppp.toString() : '',
        expected: 'x'
    })

    useHookTest({
        testName: `Tests useRouteMatch with single routeName params but expecting no match`,
        routeName,
        initialRoute: '/never/gonna/match',
        initialPath: '/never/gonna/match',
        useHook: () => useRouteMatch('path.queryParam'), // expect neither of these routes to match on the given initialPath and initialRoute
        shouldExitEarly: match => match !== null,
        handleHookResult: () => 'pass',
        expected: 'pass'
    })


    useHookTest({
        testName: `Tests useRouteMatch with RouteProps param and single routeName`,
        routeName,
        initialRoute,
        useHook: () => useRouteMatch({ routeName }),
        shouldExitEarly: match => match === null,
        handleHookResult: match => match !== null ? match.params.ppp.toString() : '',
        expected: 'x'
    })

})

describe(`useSmartRoute('some_params')`, () => {


    const routeName = 'some_params' as const
    const initialRoute = route(routeName, { param_a: 'val_param_a', param_b: 'val_param_b'  }, { query_param_a: 'val_query_param_a' })

    useHookTest({
        testName: `Gets params and query params from 'some_params' route context for 'some_params_plus', only giving required params/query params for 'some_params_plus'`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=val_query_param_a'
    })

    useHookTest({
        testName: `Overrides all params/query params from 'some_params' route context`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName),
        handleHookResult: smartRoute => {
            return smartRoute('some_params_plus', 
                    { 
                        param_a: 'newval_param_a', 
                        param_b: 'newval_param_b', 
                        param_c: 'val_param_c', 
                        definitely_mandatory_d: 'val_definitely_mandatory_d',
                    },
                    { 
                        query_param_a: 'newval_query_param_a', 
                        query_param_b: 'val_query_param_b' 
                    }
                )
            },
        expected: '/newval_param_a/newval_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=newval_query_param_a&query_param_b=val_query_param_b'
    })

    useHookTest({
        testName: `Gets params from 'some_params' route context for 'some_params_plus' but overwrites query_param_a using undefined`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }, {query_param_a: undefined}),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d'
    })

})


describe(`useSmartRoute('some_params', { preserveParams, preserveQueryParams })`, () => {

    const routeName = 'some_params' as const
    const initialRoute = route(routeName, { param_a: 'val_param_a', param_b: 'val_param_b'  }, { query_param_a: 'val_query_param_a' })

    useHookTest({
        testName: `Gets params from 'some_params' for 'some_params_plus', passing { preserveQueryParams: false }`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveQueryParams: false }),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d'
    })

    useHookTest({
        testName: `Gets params from 'some_params' for 'some_params_plus', passing { preserveQueryParams: false } and adding new queryParams`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveQueryParams: false }),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }, {query_param_b: 'hello'}),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d?query_param_b=hello'
    })


    useHookTest({
        testName: `Gets params from 'some_params' for 'some_params_plus', passing { preserveParams: true, preserveQueryParams: false }`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveParams: true, preserveQueryParams: false }),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d'
    })

    useHookTest({
        testName: `Gets params from 'some_params' for 'some_params_plus', passing { preserveParams: true, preserveQueryParams: false } and adding new queryParams`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveParams: true, preserveQueryParams: false }),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }, {query_param_b: 'hello'}),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d?query_param_b=hello'
    })

    useHookTest({
        testName: `Gets ONLY query params from 'some_params' for 'some_params_plus', passing { preserveParams: false }`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveParams: false }),
        handleHookResult: smartRoute => {
            return smartRoute(
                'some_params_plus', 
                {
                    param_a: 'newval_param_a',
                    param_b: 'newval_param_b',
                    param_c: 'val_param_c', 
                    definitely_mandatory_d: 'val_definitely_mandatory_d' 
                })
        },
        expected: '/newval_param_a/newval_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=val_query_param_a'
    })

    useHookTest({
        testName: `Gets ONLY query params from 'some_params' for 'some_params_plus', passing { preserveParams: false } but overwriting all query params anyway`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveParams: false }),
        handleHookResult: smartRoute => {
            return smartRoute(
                'some_params_plus', 
                {
                    param_a: 'newval_param_a',
                    param_b: 'newval_param_b',
                    param_c: 'val_param_c', 
                    definitely_mandatory_d: 'val_definitely_mandatory_d' 
                },
                {
                    query_param_a: 'newval_query_param_a',
                    query_param_b: 'hello'
                }
                )
        },
        expected: '/newval_param_a/newval_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=newval_query_param_a&query_param_b=hello'
    })

    useHookTest({
        testName: `Gets ONLY query params from 'some_params' for 'some_params_plus', passing { preserveParams: false, preserveQueryParams: true }`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveParams: false, preserveQueryParams: true }),
        handleHookResult: smartRoute => {
            return smartRoute(
                'some_params_plus', 
                {
                    param_a: 'newval_param_a',
                    param_b: 'newval_param_b',
                    param_c: 'val_param_c', 
                    definitely_mandatory_d: 'val_definitely_mandatory_d' 
                })
        },
        expected: '/newval_param_a/newval_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=val_query_param_a'
    })

    useHookTest({
        testName: `Gets ONLY query params from 'some_params' for 'some_params_plus', passing { preserveParams: false, preserveQueryParams: true } but overwriting all query params anyway`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(routeName, { preserveParams: false, preserveQueryParams: true }),
        handleHookResult: smartRoute => {
            return smartRoute(
                'some_params_plus', 
                {
                    param_a: 'newval_param_a',
                    param_b: 'newval_param_b',
                    param_c: 'val_param_c', 
                    definitely_mandatory_d: 'val_definitely_mandatory_d' 
                },
                {
                    query_param_a: 'newval_query_param_a',
                    query_param_b: 'hello'
                }
                )
        },
        expected: '/newval_param_a/newval_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=newval_query_param_a&query_param_b=hello'
    })

})

describe(`useSmartRoute()`, () => {

    const routeName = 'some_params' as const
    const initialRoute = route(routeName, { param_a: 'val_param_a', param_b: 'val_param_b'  }, { query_param_a: 'val_query_param_a' })

    useHookTest({
        testName: `Gets params and query params from 'some_params' for 'some_params_plus'`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute(),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=val_query_param_a'
    })

})

describe(`useSmartRoute({ preserveParams, preserveQueryParams })`, () => {

    const routeName = 'some_params' as const
    const initialRoute = route(routeName, { param_a: 'val_param_a', param_b: 'val_param_b'  }, { query_param_a: 'val_query_param_a' })

    useHookTest({
        testName: `Gets params and query params from 'some_params' for 'some_params_plus', passing { preserveParams: true }`,
        routeName,
        initialRoute,
        useHook: () => useSmartRoute({ preserveParams: true }),
        handleHookResult: smartRoute => smartRoute('some_params_plus', { param_c: 'val_param_c', definitely_mandatory_d: 'val_definitely_mandatory_d' }),
        expected: '/val_param_a/val_param_b/val_param_c/val_definitely_mandatory_d?query_param_a=val_query_param_a'
    })

})


//-----------------------Can "test" these types using the intellisense-------------------

const useSmartRouteTestFn = () => useSmartRoute('some_params')
declare const smartRouteTest: ReturnType<typeof useSmartRouteTestFn>
const some_params_plus_test = () => smartRouteTest('some_params_plus', { param_c: '', definitely_mandatory_d: '',  }) // expect it to demand param_c & definitely_mandatory_d
const covered_by_some_params_test = () => smartRouteTest('covered_by_some_params') // expect it to demand nothing, not even a 2nd param arg

const useParamsTestFn = () => useParams('some_params')
type UseParamsTestType = ReturnType<typeof useParamsTestFn>             // { param_a: string, param_b: string, param_c?: string | undefined }

const useQueryParamsTestFn = () => useQueryParams('path.queryParam.of')
type UseQueryParamsTestType = ReturnType<typeof useQueryParamsTestFn>   // { qp1?: "qv1" | "qv2" | undefined; }

type IsPreciselyEqual<A, B> = 
    [A] extends [B] 
        ? [B] extends [A]
            ? DeepRequired<A> extends DeepRequired<B>
                ? DeepRequired<B> extends DeepRequired<A>
                    ? 'Pass'
                    : 'Fail'
                : 'Fail'
            : 'Fail'
        : 'Fail'

type TestRouteParams<
    N extends RouteName,
    TExpected,
    X extends (IsPreciselyEqual<RouteParamsFor<N>, TExpected> extends "Pass" ? string : never)
> = TExpected

type TestQueryParams<
    N extends RouteName,
    TExpected,
    X extends (IsPreciselyEqual<QueryParamsFor<N>, TExpected> extends "Pass" ? string : never)
> = TExpected


type Tests = [
    TestRouteParams<'path', {}, ''>,
    TestQueryParams<'path', {}, ''>,
    TestRouteParams<'param', { p0: string }, ''>,
    TestRouteParams<'param.ofDefault', { dp0?: "abc" | "def" }, ''>,
    TestRouteParams<'numParam', { np0: number }, ''>,

    TestRouteParams<'path.param', { p1: string }, ''>,
    TestRouteParams<'path.param.of', { p2: "v1" | "v2" }, ''>,
    TestRouteParams<'path.param.of.param', { p3: "v3" | "v4", p4: string }, ''>,

    TestRouteParams<'path.numParam', { np1: number }, ''>,
    TestRouteParams<'path.numParam.numParam', { np2: number, np3: number }, ''>,
    
    TestRouteParams<'group', { pgroup: string }, ''>,
    TestQueryParams<'group', { qpgroup?: string }, ''>,
    TestRouteParams<'group.path', { pgroup: string }, ''>,
    TestQueryParams<'group.path', { qpgroup?: string }, ''>,
    TestRouteParams<'group.param', { pgroup: string, gp0: string }, ''>,
    TestRouteParams<'group.numParam', { pgroup: string, gnp0: number }, ''>,
    TestRouteParams<'group.param.queryParam', { pgroup: string, gp1: string }, ''>,

    TestQueryParams<'group.param.queryParam', { qpgroup?: string, gqp0?: string }, ''>,

    TestRouteParams<'group.nestedGroup', { pgroup: string, pnestedgroup: string }, ''>,
    TestQueryParams<'group.nestedGroup', { qpgroup?: string, qpnestedgroup?: string }, ''>,
    TestRouteParams<'group.nestedGroup.path', { pgroup: string, pnestedgroup: string }, ''>,
    TestQueryParams<'group.nestedGroup.path', { qpgroup?: string, qpnestedgroup?: string }, ''>,
    TestRouteParams<'group.nestedGroup.param', { pgroup: string, pnestedgroup: string, ngp0: string }, ''>,
    TestRouteParams<'group.nestedGroup.numParam', { pgroup: string, pnestedgroup: string, ngnp0: number }, ''>,
    TestRouteParams<'group.nestedGroup.param.queryParam', { pgroup: string, pnestedgroup: string, ngp1: string }, ''>,
    TestQueryParams<'group.nestedGroup.param.queryParam', { qpgroup?: string, qpnestedgroup?: string, ngqp0?: string }, ''>,

    TestRouteParams<'longpath', {}, ''>,

    TestQueryParams<'path.queryParam', { qp0?: string }, ''>,
    TestQueryParams<'path.queryParam.of', { qp1?: 'qv1' | 'qv2' }, ''>,

    TestRouteParams<'all', { ppp: 0 | "x" | "y", nnn: number }, ''>,

    TestQueryParams<'all', { queryp1?: string, query_missing?: string, queryp2?: 1 | 2 }, ''>,
]
