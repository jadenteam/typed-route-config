import { RouteBuilder } from './RouteBuilder'
import { RecursiveRouteConfig } from '../RouteConfig'

export type Configurer<B extends RouteBuilder<any, any>, RR extends RecursiveRouteConfig> = (builder: B) => RR