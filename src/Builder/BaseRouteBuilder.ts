import { Route } from '../Route'

const TYPE_MARKER = Symbol()

export type BaseRouteBuilder<P, Q> = {
    build: () => Route<P, Q>
    [TYPE_MARKER]: true
}

export function isRouteBuilder(t: any): t is BaseRouteBuilder<any, any> {
    return t[TYPE_MARKER]
}

export const routeBuilderTypeMarker = { [TYPE_MARKER]: true as true }