import React from 'react'
import { Link } from 'react-router-dom'

import { RouteParams, QueryParams } from '.././GetParams'
import { RouteConfig } from '.././RouteConfig'

import { UseSmartRoute, LenientSmartRoute } from '.././hooks'
import { SmartParamConfig, defaultSmartParamConfig } from '.././SmartParamConfig'

export type SmartLinkProps<R extends RouteConfig, N extends keyof R> = 
    Omit<React.ComponentProps<typeof Link>, 'to'> 
    & {
        routeName: N
        smartParamConfig?: SmartParamConfig
        params?: Partial<RouteParams<R, N>>
        queryParams?: QueryParams<R, N>
    }

export function makeSmartLink<R extends RouteConfig>(useSmartRoute: UseSmartRoute<R>) {
    return function SmartLink<N extends keyof R>({ 
        routeName, 
        smartParamConfig = defaultSmartParamConfig, 
        params, 
        queryParams, 
        ...props
    }: SmartLinkProps<R, N>) {

        // smartRoute will not play well in dynamic contexts yet, cast for now to make my lie easier
        const smartRoute = useSmartRoute(smartParamConfig as any) as LenientSmartRoute<R> 

        return <Link {...props} to={smartRoute(routeName, params, queryParams)}>{props.children}</Link>
    }
}