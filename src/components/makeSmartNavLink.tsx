import React from 'react'
import { NavLink } from 'react-router-dom'

import { RouteParams, QueryParams } from '.././GetParams'
import { RouteConfig } from '.././RouteConfig'

import { UseSmartRoute, LenientSmartRoute } from '.././hooks'
import { defaultSmartParamConfig, SmartParamConfig } from '.././SmartParamConfig'

export type SmartNavLinkProps<R extends RouteConfig, N extends keyof R> = 
    Omit<React.ComponentProps<typeof NavLink>, 'to'>
    & {
        routeName: N
        smartParamConfig?: SmartParamConfig
        params?: Partial<RouteParams<R, N>>
        queryParams?: QueryParams<R, N>
    }

export function makeSmartNavLink<R extends RouteConfig>(useSmartRoute: UseSmartRoute<R>) {
    return function SmartNavLink<N extends keyof R>({
        routeName,
        smartParamConfig = defaultSmartParamConfig,
        params, 
        queryParams,
        ...props
    }: SmartNavLinkProps<R, N>) {

        // smartRoute will not play well in dynamic contexts yet, cast for now to make my lie easier
        const smartRoute = useSmartRoute(smartParamConfig as any) as LenientSmartRoute<R> 
    
        return <NavLink {...props} to={smartRoute(routeName, params, queryParams)}>{props.children}</NavLink>
    }
}