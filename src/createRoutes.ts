import { RouteBuilder, Configurer, isRouteBuilder, routeBuilderTypeMarker } from './Builder'
import { FlattenRouteConfig, RecursiveRouteConfig } from './RouteConfig'
import { Route, emptyRoute, makeParamName } from './Route'
import { makeSmartLink, makeSmartNavLink } from './components'
import { makeUseParams, makeUseQueryParams, makeUseSmartRoute, makeUseRouteMatch } from './hooks'
import { RouteParams, QueryParams } from './GetParams'
import { RoutesAPI } from './RoutesAPI'

export function createRoutes<RR extends RecursiveRouteConfig>(configurer: Configurer<RouteBuilder<{}, {}>, RR>): RoutesAPI<FlattenRouteConfig<RR>> {
    type R = FlattenRouteConfig<RR>

    function makeRenderer<P, S extends keyof P>(render: (params: P) => string, paramName: S, defaultValue: string | number = ''): (params: P) => string {
        return params => {
            const maybeParam = params[paramName]
            return `${render(params)}/${maybeParam !== undefined ? maybeParam : defaultValue}`
        }
    }

    function builder<P, Q>(routeValues: Route<P, Q>): RouteBuilder<P, Q> {
        const { renderPath, renderRoute, renderQueryString, paramNames, queryParamNames } = routeValues

        function next<NextP extends P, NextQ extends Q>(nextRouteValues: Partial<Route<NextP, NextQ>>) {
            return builder<NextP, NextQ>({
                ...routeValues,
                ...nextRouteValues
            })
        }

        return {
            path: p => next({
                renderPath: partialParams => `${renderPath(partialParams)}/${p}`,
                renderRoute: params => `${renderRoute(params)}/${p}`
            }),

            param: <S extends string>(paramName: S) => {
                type NextP = P & { [K in S]?: string | number }; // [K in S]*?*:  and string | number wider than string to make compiler happy

                const nextRenderPath = makeRenderer<Partial<NextP>, S>(renderPath, paramName, `:${paramName}`)

                const nextBuilder = next<NextP, Q>({
                    renderPath: nextRenderPath,
                    renderRoute: params => `${renderRoute(params)}/${params[paramName]}`,
                    paramNames: [...paramNames, makeParamName(paramName)],
                })
                    
                return {
                    ...nextBuilder,
                    of: () => nextBuilder, // compiler not happy with just doing return this
                    ofDefault: defaultValue => next<NextP, Q>({
                        renderPath: nextRenderPath,
                        renderRoute: makeRenderer<NextP, S>(renderRoute, paramName, defaultValue),
                        paramNames: [...paramNames, makeParamName(paramName, true)],
                    })
                }
            },

            queryParam: <S extends string>(queryParamName: S) => {
                type NextQ = Q & { [K in S]?: string | number }

                const makeNextRenderQueryString = <V extends string | number>(defaultValue?: V) => (queryParams: NextQ) => {
                    const currentQueryString = renderQueryString(queryParams)

                    const queryParam = queryParams[queryParamName] === undefined
                        ? defaultValue
                        : queryParams[queryParamName]

                    return queryParam !== undefined 
                        ? `${currentQueryString}${currentQueryString === '' ? '?' : '&'}${queryParamName}=${encodeURIComponent(queryParam)}`
                        : currentQueryString
                }

                const nextBuilder = next<P, NextQ>({
                    renderQueryString: makeNextRenderQueryString(),
                    queryParamNames: [...queryParamNames, queryParamName],
                })

                return {
                    ...nextBuilder,
                    of: () => nextBuilder,
                    ofDefault: defaultValue => next<P, NextQ>({
                        renderQueryString: makeNextRenderQueryString(defaultValue),
                        queryParamNames: [...queryParamNames, queryParamName],
                    })
                }
            },

            numParam(paramName) {
                return this.param(paramName).of<number>()
            },

            group(groupConfigurer) { return groupConfigurer(this); },

            build: () => ({
                renderPath,
                renderRoute,
                renderQueryString,
                paramNames,
                queryParamNames
            }),
            ...routeBuilderTypeMarker
        }
    }

    const routes = flattenRecursiveRouteConfig(configurer(builder(emptyRoute)))

    const route: RoutesAPI<R>['route'] = <N extends keyof R>(routeName: N, params?: RouteParams<R, N>, queryParams?: QueryParams<R, N>) => {
        const { renderRoute, renderQueryString } = routes[routeName].build()            
        return `${renderRoute(params || {})}${queryParams ? renderQueryString(queryParams) : ''}`;
    }

    const path: RoutesAPI<R>['path'] = (routeName, partialParams) => routes[routeName].build().renderPath(partialParams || {})

    const routeNames = Object.keys(routes) as (keyof R)[]
    const isRouteName = (routeName: string | number | symbol): routeName is keyof R => routeNames.some(n => n === routeName)

    const paramNames: RoutesAPI<R>['paramNames'] = routeName => routes[routeName].build().paramNames
    const queryParamNames: RoutesAPI<R>['queryParamNames'] = routeName => routes[routeName].build().queryParamNames

    const useQueryParams = makeUseQueryParams(queryParamNames)
    const useSmartRoute = makeUseSmartRoute(route, path, paramNames, useQueryParams)


    return {
        route,
        path,
        paramNames,
        queryParamNames,
        routes,
        routeNames,
        isRouteName,
        SmartLink: makeSmartLink(useSmartRoute),
        SmartNavLink: makeSmartNavLink(useSmartRoute),
        useSmartRoute,
        useParams: makeUseParams(path, paramNames),
        useQueryParams,
        useRouteMatch: makeUseRouteMatch(path)
    }
}


function flattenRecursiveRouteConfig<RR extends RecursiveRouteConfig>(recursiveRouteConfig: RR, outerPrefix: string = ''): FlattenRouteConfig<RR> {
    return Object.entries(recursiveRouteConfig).reduce(
        (carry, [key, value]) => {
            if (!value) return carry
            const dot = (outerPrefix && key) ? '.' : ''

            const routeName = `${outerPrefix}${dot}${key}`

            if (isRouteBuilder(value)) {
                (carry as any)[routeName] = value
            } else {                
                return {
                    ...carry,
                    ...flattenRecursiveRouteConfig(value, routeName)
                }
            }
            return carry
        },
        {} as FlattenRouteConfig<RR>
    )
}
