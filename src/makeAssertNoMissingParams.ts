import { RoutesAPI } from './RoutesAPI'
import { RouteConfig } from './RouteConfig'
import { RouteParams } from './GetParams'

export function makeAssertNoMissingParams<R extends RouteConfig>(paramNames: RoutesAPI<R>['paramNames'], componentName: string) {

    return function assertNoMissingParams<N extends keyof R>(routeName: N, params: any, message?: string): asserts params is RouteParams<R, N> {

        const missingParamNames = paramNames(routeName).filter(({ name, optional }) => !(optional || (name in params)));

        if (missingParamNames.length > 0) {
            const msg = message || `Unable to get all params for routeName=${String(routeName)}.`
            throw new Error(`Error using ${componentName}. ${msg} The following params were missing ${missingParamNames.map(({ name }) => name).join(', ')}`);
        }
    }
}