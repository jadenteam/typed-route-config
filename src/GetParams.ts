import type { BaseRouteBuilder } from './Builder'
import type { RouteConfig } from './RouteConfig'

// intersecting with {} stops typescript aggressively simplifying these in the intellisense, sort of like if these were able to be interfaces
type _RouteParams<B extends BaseRouteBuilder<any, any>> = B extends BaseRouteBuilder<infer P, any> ? P : never
export type RouteParams<R extends RouteConfig, N extends keyof R> = _RouteParams<R[N]> & {}

type _QueryParams<B extends BaseRouteBuilder<any, any>> = B extends BaseRouteBuilder<any, infer Q> ? Q : never
export type QueryParams<R extends RouteConfig, N extends keyof R> = _QueryParams<R[N]> & {}

