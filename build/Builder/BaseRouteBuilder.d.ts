import { Route } from '../Route';
declare const TYPE_MARKER: unique symbol;
export type BaseRouteBuilder<P, Q> = {
    build: () => Route<P, Q>;
    [TYPE_MARKER]: true;
};
export declare function isRouteBuilder(t: any): t is BaseRouteBuilder<any, any>;
export declare const routeBuilderTypeMarker: {
    [TYPE_MARKER]: true;
};
export {};
