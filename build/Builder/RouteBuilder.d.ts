import { Configurer } from './Configurer';
import { RecursiveRouteConfig } from '../RouteConfig';
import { BaseRouteBuilder } from './BaseRouteBuilder';
type RBWith<P, Q, S extends string, T> = RouteBuilder<P & {
    [K in S]: T;
}, Q>;
export interface RouteBuilder<P, Q> extends BaseRouteBuilder<P, Q> {
    path: (p: string) => RouteBuilder<P, Q>;
    param: <S extends string>(p: S) => OfRouteBuilder<P, Q, S>;
    numParam: <S extends string>(p: S) => RBWith<P, Q, S, number>;
    queryParam: <S extends string>(q: S) => OfQueryParamBuilder<P, Q, S>;
    group: <RR extends RecursiveRouteConfig>(x: Configurer<RouteBuilder<P, Q>, RR>) => RR;
}
interface OfRouteBuilder<P, Q, S extends string> extends RBWith<P, Q, S, string> {
    of: <V extends (string | number) = string>() => RBWith<P, Q, S, V>;
    ofDefault: <V extends (string | number)>(defaultValue: V) => RouteBuilder<P & {
        [K in S]?: V;
    }, Q>;
}
type QueryPBWith<P, Q, S extends string, T> = RouteBuilder<P, Q & {
    [K in S]?: T;
}>;
interface OfQueryParamBuilder<P, Q, S extends string> extends QueryPBWith<P, Q, S, string> {
    of: <V extends (string | number) = string>() => QueryPBWith<P, Q, S, V>;
    ofDefault: <V extends (string | number)>(defaultValue: V) => QueryPBWith<P, Q, S, V>;
}
export {};
