import { RouteBuilder, Configurer } from './Builder';
import { FlattenRouteConfig, RecursiveRouteConfig } from './RouteConfig';
import { RoutesAPI } from './RoutesAPI';
export declare function createRoutes<RR extends RecursiveRouteConfig>(configurer: Configurer<RouteBuilder<{}, {}>, RR>): RoutesAPI<FlattenRouteConfig<RR>>;
