export type ParamName = {
    name: string;
    optional: boolean;
};
export type Route<P, Q> = {
    renderPath: (partialParams: Partial<P>) => string;
    renderRoute: (params: P) => string;
    renderQueryString: (queryParams: Q) => string;
    paramNames: ParamName[];
    queryParamNames: (keyof Q)[];
};
export declare const emptyRoute: Route<{}, {}>;
export declare const makeParamName: (name: string, optional?: boolean) => {
    name: string;
    optional: boolean;
};
