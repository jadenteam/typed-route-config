import React from 'react';
import { Link, NavLink, useMatch, useSearchParams, useParams } from 'react-router-dom';

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __spreadArray(to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
}

var _a;
var TYPE_MARKER = Symbol();
function isRouteBuilder(t) {
    return t[TYPE_MARKER];
}
var routeBuilderTypeMarker = (_a = {}, _a[TYPE_MARKER] = true, _a);

var emptyRoute = {
    renderPath: function () { return ''; },
    renderRoute: function () { return ''; },
    renderQueryString: function () { return ''; },
    paramNames: [],
    queryParamNames: []
};
var makeParamName = function (name, optional) {
    if (optional === void 0) { optional = false; }
    return ({ name: name, optional: optional });
};

var defaultSmartParamConfig = {
    preserveParams: true,
    preserveQueryParams: true
};

function makeSmartLink(useSmartRoute) {
    return function SmartLink(_a) {
        var routeName = _a.routeName, _b = _a.smartParamConfig, smartParamConfig = _b === void 0 ? defaultSmartParamConfig : _b, params = _a.params, queryParams = _a.queryParams, props = __rest(_a, ["routeName", "smartParamConfig", "params", "queryParams"]);
        // smartRoute will not play well in dynamic contexts yet, cast for now to make my lie easier
        var smartRoute = useSmartRoute(smartParamConfig);
        return React.createElement(Link, __assign({}, props, { to: smartRoute(routeName, params, queryParams) }), props.children);
    };
}

function makeSmartNavLink(useSmartRoute) {
    return function SmartNavLink(_a) {
        var routeName = _a.routeName, _b = _a.smartParamConfig, smartParamConfig = _b === void 0 ? defaultSmartParamConfig : _b, params = _a.params, queryParams = _a.queryParams, props = __rest(_a, ["routeName", "smartParamConfig", "params", "queryParams"]);
        // smartRoute will not play well in dynamic contexts yet, cast for now to make my lie easier
        var smartRoute = useSmartRoute(smartParamConfig);
        return React.createElement(NavLink, __assign({}, props, { to: smartRoute(routeName, params, queryParams) }), props.children);
    };
}

function makeAssertNoMissingParams(paramNames, componentName) {
    return function assertNoMissingParams(routeName, params, message) {
        var missingParamNames = paramNames(routeName).filter(function (_a) {
            var name = _a.name, optional = _a.optional;
            return !(optional || (name in params));
        });
        if (missingParamNames.length > 0) {
            var msg = message || "Unable to get all params for routeName=".concat(String(routeName), ".");
            throw new Error("Error using ".concat(componentName, ". ").concat(msg, " The following params were missing ").concat(missingParamNames.map(function (_a) {
                var name = _a.name;
                return name;
            }).join(', ')));
        }
    };
}

function makeUseParams(path, paramNames) {
    //https://github.com/microsoft/TypeScript/issues/34523
    var fn = makeAssertNoMissingParams(paramNames, 'useParams');
    var assertNoMissingParams = fn;
    return function useParams(routeName, throwOnMissingParams) {
        if (throwOnMissingParams === void 0) { throwOnMissingParams = false; }
        var match = useMatch({
            path: path(routeName),
            end: false
        });
        var paramsFromContext = (match === null || match === void 0 ? void 0 : match.params) || {};
        if (throwOnMissingParams) {
            assertNoMissingParams(routeName, paramsFromContext);
        }
        return paramsFromContext;
    };
}

function makeUseQueryParams(queryParamNames) {
    return function useQueryParams(routeName) {
        var reduceUrlSearchParams = function (urlSearchParams) {
            var names = routeName ? queryParamNames(routeName) : Array.from(urlSearchParams.keys());
            return names.reduce(function (result, queryParamName) {
                var queryParamNameStr = queryParamName.toString();
                if (urlSearchParams.has(queryParamNameStr)) {
                    var queryParam = urlSearchParams.get(queryParamNameStr);
                    if (queryParam !== null) {
                        result[queryParamName] = queryParam;
                    }
                }
                return result;
            }, {});
        };
        var _a = useSearchParams(), urlSearchParams = _a[0], setUrlSearchParams = _a[1];
        return [
            reduceUrlSearchParams(urlSearchParams),
            function (nextInit, options) { return setUrlSearchParams(function (prevUrlSearchParams) {
                var prevParams = reduceUrlSearchParams(prevUrlSearchParams);
                var newParams = typeof nextInit === 'function'
                    ? nextInit(prevParams)
                    : nextInit;
                var nextParams = (options === null || options === void 0 ? void 0 : options.mergeParams)
                    ? __assign(__assign({}, prevParams), newParams) : newParams;
                return nextParams;
            }, options); }
        ];
    };
}

function makeUseRouteMatch(path) {
    return function useRouteMatch(routeNameOrPattern) {
        var routeMatchArg = typeof routeNameOrPattern !== 'object'
            ? {
                path: path(routeNameOrPattern),
                routeName: routeNameOrPattern
            }
            : __assign(__assign({}, routeNameOrPattern), { path: path(routeNameOrPattern.routeName) });
        var match = useMatch(routeMatchArg);
        if (match === null)
            return null;
        // here we know we have matchedRouteName = 'routeName1' | 'routeName2' and match.params = params1 | params2
        // but we need to assert that if we have 'routeName1' then we have params1 and if 'routeName2' then params2
        // could do some fancy checks using paramNames etc. but for now just assert its the case
        // first assign to something that pretty much looks like the result,
        // but without the generic context so we can at least get an error if we miss one of the properties
        var result = __assign(__assign({}, match), { routeName: routeMatchArg.routeName });
        // now just cast it
        return result;
    };
}

function makeUseSmartRoute(route, path, paramNames, useQueryParams) {
    //https://github.com/microsoft/TypeScript/issues/34523
    var fn = makeAssertNoMissingParams(paramNames, 'useSmartRoute');
    var assertNoMissingParams = fn;
    return function useSmartRoute(currentRouteNameOrSmartParamConfig, maybeSmartParamConfig) {
        var currentRouteName = typeof currentRouteNameOrSmartParamConfig === 'string'
            ? currentRouteNameOrSmartParamConfig
            : undefined;
        var _a = maybeSmartParamConfig || (typeof currentRouteNameOrSmartParamConfig === 'object'
            ? currentRouteNameOrSmartParamConfig
            : {}), _b = _a.preserveParams, preserveParams = _b === void 0 ? defaultSmartParamConfig.preserveParams : _b, _c = _a.preserveQueryParams, preserveQueryParams = _c === void 0 ? defaultSmartParamConfig.preserveQueryParams : _c;
        // we need to handle both scenarios: where they provided the current route name and where they did not
        // if they did provide it, then we can use that to perform the match using useMatch
        // if not, then we rely on useParamsReactRouter
        // but since this is a hook, we must call both hooks every render for safety
        var match = useMatch({
            path: currentRouteName ? path(currentRouteName) : '',
            end: false
        });
        var paramsUsingRouteName = (match === null || match === void 0 ? void 0 : match.params) || {};
        var paramsFromContext = useParams();
        var applicableParams = currentRouteName ? paramsUsingRouteName : paramsFromContext;
        var queryParamsFromContext = useQueryParams(currentRouteName)[0]; // useQueryParams is capable of handling any routeName (including undefined) the type is just slightly more restrictive
        return function smartRoute(routeName, givenParams, givenQueryParams) {
            var params = __assign(__assign({}, (preserveParams ? applicableParams : {})), (givenParams || {}));
            var msg = currentRouteName ? "Unable to get all params from routeName=".concat(String(currentRouteName), " for routeName=").concat(String(routeName)) : '';
            assertNoMissingParams(routeName, params, msg);
            var queryParams = __assign(__assign({}, (preserveQueryParams ? queryParamsFromContext : {})), (givenQueryParams || {}));
            return route(routeName, params, queryParams);
        };
    };
}

function createRoutes(configurer) {
    function makeRenderer(render, paramName, defaultValue) {
        if (defaultValue === void 0) { defaultValue = ''; }
        return function (params) {
            var maybeParam = params[paramName];
            return "".concat(render(params), "/").concat(maybeParam !== undefined ? maybeParam : defaultValue);
        };
    }
    function builder(routeValues) {
        var renderPath = routeValues.renderPath, renderRoute = routeValues.renderRoute, renderQueryString = routeValues.renderQueryString, paramNames = routeValues.paramNames, queryParamNames = routeValues.queryParamNames;
        function next(nextRouteValues) {
            return builder(__assign(__assign({}, routeValues), nextRouteValues));
        }
        return __assign({ path: function (p) { return next({
                renderPath: function (partialParams) { return "".concat(renderPath(partialParams), "/").concat(p); },
                renderRoute: function (params) { return "".concat(renderRoute(params), "/").concat(p); }
            }); }, param: function (paramName) {
                var nextRenderPath = makeRenderer(renderPath, paramName, ":".concat(paramName));
                var nextBuilder = next({
                    renderPath: nextRenderPath,
                    renderRoute: function (params) { return "".concat(renderRoute(params), "/").concat(params[paramName]); },
                    paramNames: __spreadArray(__spreadArray([], paramNames, true), [makeParamName(paramName)], false),
                });
                return __assign(__assign({}, nextBuilder), { of: function () { return nextBuilder; }, ofDefault: function (defaultValue) { return next({
                        renderPath: nextRenderPath,
                        renderRoute: makeRenderer(renderRoute, paramName, defaultValue),
                        paramNames: __spreadArray(__spreadArray([], paramNames, true), [makeParamName(paramName, true)], false),
                    }); } });
            }, queryParam: function (queryParamName) {
                var makeNextRenderQueryString = function (defaultValue) { return function (queryParams) {
                    var currentQueryString = renderQueryString(queryParams);
                    var queryParam = queryParams[queryParamName] === undefined
                        ? defaultValue
                        : queryParams[queryParamName];
                    return queryParam !== undefined
                        ? "".concat(currentQueryString).concat(currentQueryString === '' ? '?' : '&').concat(queryParamName, "=").concat(encodeURIComponent(queryParam))
                        : currentQueryString;
                }; };
                var nextBuilder = next({
                    renderQueryString: makeNextRenderQueryString(),
                    queryParamNames: __spreadArray(__spreadArray([], queryParamNames, true), [queryParamName], false),
                });
                return __assign(__assign({}, nextBuilder), { of: function () { return nextBuilder; }, ofDefault: function (defaultValue) { return next({
                        renderQueryString: makeNextRenderQueryString(defaultValue),
                        queryParamNames: __spreadArray(__spreadArray([], queryParamNames, true), [queryParamName], false),
                    }); } });
            }, numParam: function (paramName) {
                return this.param(paramName).of();
            }, group: function (groupConfigurer) { return groupConfigurer(this); }, build: function () { return ({
                renderPath: renderPath,
                renderRoute: renderRoute,
                renderQueryString: renderQueryString,
                paramNames: paramNames,
                queryParamNames: queryParamNames
            }); } }, routeBuilderTypeMarker);
    }
    var routes = flattenRecursiveRouteConfig(configurer(builder(emptyRoute)));
    var route = function (routeName, params, queryParams) {
        var _a = routes[routeName].build(), renderRoute = _a.renderRoute, renderQueryString = _a.renderQueryString;
        return "".concat(renderRoute(params || {})).concat(queryParams ? renderQueryString(queryParams) : '');
    };
    var path = function (routeName, partialParams) { return routes[routeName].build().renderPath(partialParams || {}); };
    var routeNames = Object.keys(routes);
    var isRouteName = function (routeName) { return routeNames.some(function (n) { return n === routeName; }); };
    var paramNames = function (routeName) { return routes[routeName].build().paramNames; };
    var queryParamNames = function (routeName) { return routes[routeName].build().queryParamNames; };
    var useQueryParams = makeUseQueryParams(queryParamNames);
    var useSmartRoute = makeUseSmartRoute(route, path, paramNames, useQueryParams);
    return {
        route: route,
        path: path,
        paramNames: paramNames,
        queryParamNames: queryParamNames,
        routes: routes,
        routeNames: routeNames,
        isRouteName: isRouteName,
        SmartLink: makeSmartLink(useSmartRoute),
        SmartNavLink: makeSmartNavLink(useSmartRoute),
        useSmartRoute: useSmartRoute,
        useParams: makeUseParams(path, paramNames),
        useQueryParams: useQueryParams,
        useRouteMatch: makeUseRouteMatch(path)
    };
}
function flattenRecursiveRouteConfig(recursiveRouteConfig, outerPrefix) {
    if (outerPrefix === void 0) { outerPrefix = ''; }
    return Object.entries(recursiveRouteConfig).reduce(function (carry, _a) {
        var key = _a[0], value = _a[1];
        if (!value)
            return carry;
        var dot = (outerPrefix && key) ? '.' : '';
        var routeName = "".concat(outerPrefix).concat(dot).concat(key);
        if (isRouteBuilder(value)) {
            carry[routeName] = value;
        }
        else {
            return __assign(__assign({}, carry), flattenRecursiveRouteConfig(value, routeName));
        }
        return carry;
    }, {});
}

export { createRoutes };
//# sourceMappingURL=index.esm.js.map
