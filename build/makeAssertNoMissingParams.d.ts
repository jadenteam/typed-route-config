import { RoutesAPI } from './RoutesAPI';
import { RouteConfig } from './RouteConfig';
import { RouteParams } from './GetParams';
export declare function makeAssertNoMissingParams<R extends RouteConfig>(paramNames: RoutesAPI<R>['paramNames'], componentName: string): <N extends keyof R>(routeName: N, params: any, message?: string) => asserts params is RouteParams<R, N>;
