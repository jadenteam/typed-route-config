import { RoutesAPI } from '.././RoutesAPI';
import { RouteParams, QueryParams } from '.././GetParams';
import { RouteConfig } from '.././RouteConfig';
import { RequiredKeys, OptionalKeys, CleanIntellisense } from '.././typescript';
import { UseQueryParams } from './makeUseQueryParams';
import { PreserveBothParams, PreserveParamsOnly, PreserveQueryParamsOnly, PreserveNeitherParams } from '.././SmartParamConfig';
type KeysThatCanBeOptional<TCurrentParams, TNextParams> = Extract<keyof TNextParams, OptionalKeys<TNextParams> | RequiredKeys<TCurrentParams>>;
type _PartialOfParamsWeAlreadyHave<TCurrentParams, TNextParams> = CleanIntellisense<{
    [K in KeysThatCanBeOptional<TCurrentParams, TNextParams>]?: TNextParams[K];
} & {
    [K in Exclude<keyof TNextParams, KeysThatCanBeOptional<TCurrentParams, TNextParams>>]: TNextParams[K];
}>;
type PartialOfParamsWeAlreadyHave<R extends RouteConfig, TCurrentRouteName extends keyof R, N extends keyof R> = _PartialOfParamsWeAlreadyHave<RouteParams<R, TCurrentRouteName>, // these are the params we already have (from current route context)
RouteParams<R, N>>;
type KeysOfFullyOptional<R extends RouteConfig, TCurrentRouteName extends keyof R> = {
    [N in keyof R]: {} extends PartialOfParamsWeAlreadyHave<R, TCurrentRouteName, N> ? N : never;
}[keyof R];
type SmartRoute<R extends RouteConfig, TCurrentRouteName extends keyof R> = {
    <N extends keyof R>(routeName: N, givenParams: PartialOfParamsWeAlreadyHave<R, TCurrentRouteName, N>, givenQueryParams?: QueryParams<R, N>): string;
    <N extends KeysOfFullyOptional<R, TCurrentRouteName>>(routeName: N): string;
};
export type LenientSmartRoute<R extends RouteConfig> = <N extends keyof R>(routeName: N, givenParams?: Partial<RouteParams<R, N>>, givenQueryParams?: QueryParams<R, N>) => string;
type StrictSmartRoute<R extends RouteConfig> = <N extends keyof R>(routeName: N, givenParams: RouteParams<R, N>, givenQueryParams?: QueryParams<R, N>) => string;
export type UseSmartRoute<R extends RouteConfig> = {
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName): SmartRoute<R, TCurrentRouteName>;
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName, smartParamConfig: PreserveBothParams | PreserveParamsOnly): SmartRoute<R, TCurrentRouteName>;
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName, smartParamConfig: PreserveQueryParamsOnly): StrictSmartRoute<R>;
    (): LenientSmartRoute<R>;
    (smartParamConfig: PreserveBothParams | PreserveParamsOnly): LenientSmartRoute<R>;
    (smartParamConfig: PreserveQueryParamsOnly): StrictSmartRoute<R>;
    (smartParamConfig: PreserveNeitherParams): unknown;
    <TCurrentRouteName extends keyof R>(currentRouteName: TCurrentRouteName, smartParamConfig: PreserveNeitherParams): unknown;
};
export declare function makeUseSmartRoute<R extends RouteConfig>(route: RoutesAPI<R>['route'], path: RoutesAPI<R>['path'], paramNames: RoutesAPI<R>['paramNames'], useQueryParams: UseQueryParams<R>): UseSmartRoute<R>;
export {};
