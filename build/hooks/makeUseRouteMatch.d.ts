import { PathPattern as PathPatternReactRouter, PathMatch } from 'react-router-dom';
import { RouteConfig } from '../RouteConfig';
import { RouteParams } from '../GetParams';
import { Modify } from '../typescript';
import { RoutesAPI } from '../RoutesAPI';
type PathPattern = Omit<PathPatternReactRouter, 'path'>;
type RouteArg<R extends RouteConfig, M extends keyof R> = {
    [N in keyof R]: {
        routeName: N;
        pattern: PathPattern;
        params: RouteParams<R, N>;
    };
}[M];
type TypedPathMatch<R extends RouteConfig, N extends keyof R> = Modify<PathMatch, RouteArg<R, N>>;
type TypedPathPattern<R extends RouteConfig, N extends keyof R> = Modify<PathPattern, {
    routeName: N;
}>;
export type UseRouteMatch<R extends RouteConfig, N1 extends keyof R = keyof R> = <N extends N1>(routeNameOrPattern: N | TypedPathPattern<R, N>) => TypedPathMatch<R, N> | null;
export declare function makeUseRouteMatch<R extends RouteConfig>(path: RoutesAPI<R>['path']): UseRouteMatch<R>;
export {};
