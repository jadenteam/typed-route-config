import { NavigateOptions } from 'react-router-dom';
import { RoutesAPI } from '.././RoutesAPI';
import { QueryParams } from '.././GetParams';
import { RouteConfig } from '.././RouteConfig';
import { IfHasKeys } from '../typescript';
export type KeysWithQueryParams<R extends RouteConfig> = {
    [N in keyof R]: IfHasKeys<QueryParams<R, N>, N, never>;
}[keyof R];
type StringParams = {
    [queryParamName: string]: string | undefined;
};
export type UseQueryParams<R extends RouteConfig> = {
    <N extends KeysWithQueryParams<R>>(routeName: N): [QueryParams<R, N>, SetURLSearchParams<QueryParams<R, N>>];
    <T extends StringParams = StringParams>(): [T, SetURLSearchParams<T>];
};
type Options = NavigateOptions & {
    mergeParams?: boolean;
};
type SetURLSearchParams<P> = (nextInit?: Partial<P> | ((prev: Partial<P>) => Partial<P>), options?: Options) => void;
export declare function makeUseQueryParams<R extends RouteConfig>(queryParamNames: RoutesAPI<R>['queryParamNames']): UseQueryParams<R>;
export {};
