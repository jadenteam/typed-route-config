import { RoutesAPI } from '.././RoutesAPI';
import { RouteParams } from '.././GetParams';
import { RouteConfig } from '.././RouteConfig';
import { IfHasKeys } from '../typescript';
type KeysWithParams<R extends RouteConfig> = {
    [N in keyof R]: IfHasKeys<RouteParams<R, N>, N, never>;
}[keyof R];
export type UseParams<R extends RouteConfig> = <N extends KeysWithParams<R>>(routeName: N, throwOnMissingParams?: boolean) => RouteParams<R, N>;
export declare function makeUseParams<R extends RouteConfig>(path: RoutesAPI<R>['path'], paramNames: RoutesAPI<R>['paramNames']): UseParams<R>;
export {};
