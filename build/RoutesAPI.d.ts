/// <reference types="react" />
import { RouteConfig } from './RouteConfig';
import { RouteParams, QueryParams } from './GetParams';
import { ParamName } from './Route';
import { SmartLinkProps, SmartNavLinkProps } from './components';
import { UseSmartRoute, UseRouteMatch, UseQueryParams, UseParams } from './hooks';
type NoMandatoryParamKeys<R extends RouteConfig> = {
    [N in keyof R]: {} extends RouteParams<R, N> ? N : never;
}[keyof R];
export interface RoutesAPI<R extends RouteConfig, TRouteName extends keyof R = keyof R> {
    route<N extends TRouteName>(routeName: N, params: RouteParams<R, N>, queryParams?: QueryParams<R, N>): string;
    route<N extends NoMandatoryParamKeys<R>>(routeName: N): string;
    path: <N extends TRouteName>(routeName: N, partialParams?: Partial<RouteParams<R, N>>) => string;
    paramNames: <N extends TRouteName>(routeName: N) => ParamName[];
    queryParamNames: <N extends TRouteName>(routeName: N) => (keyof QueryParams<R, N>)[];
    routes: R;
    routeNames: TRouteName[];
    isRouteName: (routeName: string | number | symbol) => routeName is TRouteName;
    SmartLink: <N extends TRouteName>(props: SmartLinkProps<R, N>) => JSX.Element;
    SmartNavLink: <N extends TRouteName>(props: SmartNavLinkProps<R, N>) => JSX.Element;
    useSmartRoute: UseSmartRoute<R>;
    useParams: UseParams<R>;
    useQueryParams: UseQueryParams<R>;
    useRouteMatch: UseRouteMatch<R, TRouteName>;
}
export {};
