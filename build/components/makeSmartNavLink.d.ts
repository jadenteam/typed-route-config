import React from 'react';
import { NavLink } from 'react-router-dom';
import { RouteParams, QueryParams } from '.././GetParams';
import { RouteConfig } from '.././RouteConfig';
import { UseSmartRoute } from '.././hooks';
import { SmartParamConfig } from '.././SmartParamConfig';
export type SmartNavLinkProps<R extends RouteConfig, N extends keyof R> = Omit<React.ComponentProps<typeof NavLink>, 'to'> & {
    routeName: N;
    smartParamConfig?: SmartParamConfig;
    params?: Partial<RouteParams<R, N>>;
    queryParams?: QueryParams<R, N>;
};
export declare function makeSmartNavLink<R extends RouteConfig>(useSmartRoute: UseSmartRoute<R>): <N extends keyof R>({ routeName, smartParamConfig, params, queryParams, ...props }: SmartNavLinkProps<R, N>) => JSX.Element;
