import React from 'react';
import { Link } from 'react-router-dom';
import { RouteParams, QueryParams } from '.././GetParams';
import { RouteConfig } from '.././RouteConfig';
import { UseSmartRoute } from '.././hooks';
import { SmartParamConfig } from '.././SmartParamConfig';
export type SmartLinkProps<R extends RouteConfig, N extends keyof R> = Omit<React.ComponentProps<typeof Link>, 'to'> & {
    routeName: N;
    smartParamConfig?: SmartParamConfig;
    params?: Partial<RouteParams<R, N>>;
    queryParams?: QueryParams<R, N>;
};
export declare function makeSmartLink<R extends RouteConfig>(useSmartRoute: UseSmartRoute<R>): <N extends keyof R>({ routeName, smartParamConfig, params, queryParams, ...props }: SmartLinkProps<R, N>) => JSX.Element;
