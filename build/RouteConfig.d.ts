import { BaseRouteBuilder } from './Builder';
import { CleanIntellisense, UnionToIntersection } from './typescript';
export type RouteConfig = {
    [routeName: string]: BaseRouteBuilder<any, any>;
};
export type RecursiveRouteConfig = {
    [routeNameOrPrefix: string]: BaseRouteBuilder<any, any> | RecursiveRouteConfig | undefined;
    ''?: BaseRouteBuilder<any, any>;
};
export type FlattenRouteConfig<RR extends RecursiveRouteConfig> = CleanIntellisense<_FlattenRouteConfig<RR>>;
type _FlattenRouteConfig<RR extends RecursiveRouteConfig, F = MakeFlattenedRouteConfig<RR>> = {
    [N in keyof F]: AssertIsBaseRouteBuilder<F[N]>;
};
type AssertIsBaseRouteBuilder<T> = T extends BaseRouteBuilder<any, any> ? T : any;
type MakeRouteName<TGroupPrefix extends string, K extends string> = (TGroupPrefix extends '' ? K : K extends '' ? TGroupPrefix : `${TGroupPrefix}.${K}`);
type MakeEntry<TGroupPrefix extends string, K extends string, B extends BaseRouteBuilder<any, any>> = (MakeRouteName<TGroupPrefix, K> extends infer TRouteName extends string ? TRouteName extends '' ? never : Record<TRouteName, B> : never);
type MakeFlattenedRouteConfig<RR extends RecursiveRouteConfig> = (UnionToIntersection<_MakeFlattenedRouteConfig<RR>>);
type _MakeFlattenedRouteConfig<RR extends RecursiveRouteConfig, TGroupPrefix extends string = '', K extends string = keyof RR & string> = (K extends keyof RR ? RR[K] extends BaseRouteBuilder<any, any> ? MakeEntry<TGroupPrefix, K, RR[K]> : RR[K] extends RecursiveRouteConfig ? _MakeFlattenedRouteConfig<RR[K], MakeRouteName<TGroupPrefix, K>> : never : never);
export {};
