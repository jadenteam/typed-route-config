export type SmartParamConfig = {
    preserveParams?: boolean;
    preserveQueryParams?: boolean;
};
export declare const defaultSmartParamConfig: {
    readonly preserveParams: true;
    readonly preserveQueryParams: true;
};
export type PreserveBothParams = {
    preserveParams?: true;
    preserveQueryParams?: true;
};
export type PreserveParamsOnly = {
    preserveParams?: true;
    preserveQueryParams: false;
};
export type PreserveQueryParamsOnly = {
    preserveParams: false;
    preserveQueryParams?: true;
};
export type PreserveNeitherParams = {
    preserveParams: false;
    preserveQueryParams: false;
};
